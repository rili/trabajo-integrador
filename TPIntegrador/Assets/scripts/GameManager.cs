using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static float cuenta;
    public static List<PlayerData> topRecords = new List<PlayerData>();
    public TMPro.TMP_Text textocontadorconometro;
    public TMPro.TMP_Text textorecord;
    public TMPro.TMP_InputField inputField;

    // Start is called before the first frame update
    void Start()
    {
        LoadTopRecords();
        cuenta = 0;
        StartCoroutine(Comenzarcontador());
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        setearTextos();
        if (Colisionesplayer.pausa == true)
        {
            GuardarTopRecords();
        }
    }

    public IEnumerator Comenzarcontador()
    {
        while (Colisionesplayer.pausa != true)
        {
            Debug.Log("pasaron " + cuenta + " segundos.");
            yield return new WaitForSeconds(1.0f);
            cuenta++;
        }
    }

    private void GuardarTopRecords()
    {
        inputField.gameObject.SetActive(true);
    }

    public static void SaveTopRecords()
    {
        SaveManager.SavePlayerData(topRecords);
    }

    private void LoadTopRecords()
    {
        topRecords = SaveManager.LoadPlayerData();
        if (topRecords == null)
        {
            // Si no hay datos guardados, se inicializa una lista vac�a
            topRecords = new List<PlayerData>();
        }
    }

    public void ResetTopRecords()
    {
        topRecords.Clear(); // Limpiamos la lista de registros
        SaveTopRecords(); // Guardamos la lista vac�a en el archivo de guardado
    }

    private void setearTextos()
    {
        textocontadorconometro.text = "Tiempo: " + cuenta.ToString();
        string recordText = "Record: ";
        for (int i = 0; i < topRecords.Count; i++)
        {
            recordText += "\n" + topRecords[i].playerName + ": " + topRecords[i].record.ToString();
        }
        textorecord.text = recordText;
    }
}
