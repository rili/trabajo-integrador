using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    public float speed;
    public float tiempo;
    public float primera_linea;
    public float segunda_linea; 
    public float tercera_linea;
    public float multiplicador1;
    public float multiplicador2;
    public float multiplicador3;

    // Start is called before the first frame update
    void Start()
    {
        tiempo = 0;
    }

    // Update is called once per frame
    void Update()
    {
 
        tiempo++;
        if (tiempo < primera_linea)
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }
        if (tiempo > primera_linea)
        {
            transform.Translate(Vector3.up * speed *multiplicador1 * Time.deltaTime);
        }
        if (tiempo > segunda_linea)
        {
            transform.Translate(Vector3.up * speed * multiplicador2 * Time.deltaTime);
        }
        if (tiempo > tercera_linea)
        {
            transform.Translate(Vector3.up * speed * multiplicador3 * Time.deltaTime);
        }


    }
}
