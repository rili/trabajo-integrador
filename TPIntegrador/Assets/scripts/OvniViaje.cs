using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvniViaje : MonoBehaviour
{
    public int NumDir;
    public int Velocidad;
    public bool Derecha;
    public float teleportIZQ;
    public float teleportDER;

    // Start is called before the first frame update
    void Start()
    {
        NumDir = Random.Range(1, 2);
        Velocidad = Random.Range(1, 12);
        if (NumDir == 1)
        {
            Derecha = true;
        }
        if (NumDir == 2)
        {
            Derecha = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Derecha)
        {
  
            transform.Translate(Vector3.right * Velocidad * Time.deltaTime);
        }
        else
        {

            transform.Translate(Vector3.left * Velocidad * Time.deltaTime);
        }
        if (transform.position.x > teleportDER)
        {
            Derecha = false;
        }
        else if(transform.position.x < teleportIZQ)
        {
            Derecha = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ParedInvisible") == true)
        {

            Derecha = !Derecha;

        }

        if (other.gameObject.CompareTag("ParedInvisible2") == true)
        {
            Derecha = !Derecha;
        }
    }
}
