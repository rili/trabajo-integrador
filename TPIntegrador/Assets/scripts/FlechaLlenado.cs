using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FlechaLlenado : MonoBehaviour
{
    public Image flecha;
    public float lerpvel;
    public float llenact;
    public float maxllen;
    public float minllen;

    // Start is called before the first frame update
    void Start()
    {
        llenact = 0;
    }

    // Update is called once per frame
    void Update()
    {
        llenact++;
        lerpvel = (3f * Time.deltaTime);
        llenadorflecha();
        if (llenact >= maxllen)
        {
            llenact = minllen;
        }
    }

    public void llenadorflecha()
    {
        flecha.fillAmount = Mathf.Lerp(flecha.fillAmount, (llenact / maxllen), lerpvel);
    }
}
