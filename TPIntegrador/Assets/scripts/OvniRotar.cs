using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvniRotar : MonoBehaviour
{
    public bool rotado;
    // Start is called before the first frame update
    void Start()
    {
        rotado = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (HijoPersonaje.hijo == 1 && (rotado == false))
        {
            transform.Rotate(Vector3.right, 180f);
            rotado = true;
        }
        else if (HijoPersonaje.hijo == 1 && (rotado == true))
        {
            return;
        }
        else
        {
            // rotado = false;
            return;
        }
    }
}
