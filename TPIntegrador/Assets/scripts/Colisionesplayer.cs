using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colisionesplayer : MonoBehaviour
{
    public GameObject pantallagameover;
    public GameObject flecha1;
    public GameObject flecha2;
    public GameObject camara;
    public GameObject nivelador;
    public GameObject yo;
    public GameObject lava;

    private Vector3 originalPosition;
    private Vector3 InicioPosicion;
    private Vector3 InicioLava;
    private Quaternion originalRotation;
    private Quaternion originalRotationF1;
    private Quaternion originalRotationF2;
    public int Ran;
    static public int cam;
    static public bool pausa;
    public float shakeTimer;
    public float shakeMagnitude = 0.1f;
    public float shakeDuration = 2f;
    public bool shakeado;
    public float elapsedtime;
    public float tiempoespera;
    public int nivelescreados;
    public float distancianivel;
    private Rigidbody rb;
    private Vector3 savedVelocity;
    


    void Start()
    {
        InicioPosicion = yo.transform.position;
        InicioLava = lava.transform.position;
        nivelescreados = 0;
        elapsedtime = 0f;
        pausa = false;
        Time.timeScale = 1;
        rb = GetComponent<Rigidbody>();
        originalRotation = camara.transform.rotation;
        originalRotationF1 = flecha1.transform.rotation;
        originalRotationF2 = flecha2.transform.rotation;
        pantallagameover.SetActive(false);
        shakeado = false;
        shakeTimer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        originalPosition = camara.transform.position;
        if (shakeTimer > 0)
        {
            // Generar una posici�n de shake aleatoria dentro de la magnitud especificada
            Vector3 shakePosition = originalPosition + Random.insideUnitSphere * shakeMagnitude;

            // Asignar la posici�n de shake a la c�mara (u otro objeto)
            camara.transform.position = shakePosition;

            // Actualizar el temporizador
            shakeTimer -= Time.deltaTime;

            shakeado = true;
        }

        else if (shakeTimer <= 0 && (shakeado == true))
        {
            gameover();
        }

        else
        {
            // Restaurar la posici�n original
            camara.transform.position = originalPosition;
        }



        elapsedtime++;



    }

    public void StartShake()
    {
        shakeTimer = shakeDuration;
    }

    public void gameover()
    {
        GestorAudio.instancia.ReproducirSonido("OOF");
        pausa = true;
        pantallagameover.SetActive(true);
        Time.timeScale = 0;
    }

    private void getcamara()
    {
        float ahoraRotation = camara.transform.rotation.eulerAngles.z;
        if (ahoraRotation == 0 || (ahoraRotation == (0 - 180)))
        {
            cam = 1;
        }
        if (ahoraRotation == 90 || (ahoraRotation == (90 - 180)))
        {
            cam = 2;
        }
        if (ahoraRotation == 180 || (ahoraRotation == (180 - 180)))
        {
            cam = 3;
        }
        if (ahoraRotation == 270 || (ahoraRotation == (270 - 180)))
        {
            cam = 4;
        }
    }


    private void invertircamara()
    {
        float currentRotation = camara.transform.rotation.eulerAngles.z;
        float currentRotationF1 = flecha1.transform.rotation.eulerAngles.z;
        float currentRotationF2 = flecha2.transform.rotation.eulerAngles.z;

        // Calcular la rotaci�n invertida
        float invertedRotation = 180f - currentRotation;
        float invertedRotationF1 = -1f *  currentRotationF1;
        float invertedRotationF2 = -1f * currentRotationF2;

        // Aplicar la rotaci�n invertida a la c�mara
        camara.transform.rotation = Quaternion.Euler(0f, 0f, invertedRotation);
        flecha1.transform.rotation = Quaternion.Euler(0f, 0f, invertedRotationF1);
        flecha2.transform.rotation = Quaternion.Euler(0f, 0f, invertedRotationF2);
    }

    private void resetcamara()
    {
        camara.transform.rotation = originalRotation;
        flecha1.transform.rotation = originalRotationF1;
        flecha2.transform.rotation = originalRotationF2;
    }

    private void randomcamara()
    {
        Ran = Random.Range(1, 5);

        switch (Ran)
        {
            case 1:
                camara.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                break;
            case 2:
                camara.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
                break;
            case 3:
                camara.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
                break;
            case 4:
                camara.transform.rotation = Quaternion.Euler(0f, 0f, 270f);
                break;
            default:
                Debug.Log("numero invalido");
                break;
        }
    }


    private void crearnivel()
    {
        if (nivelescreados == 0)
        {
            EditorDeNivel.yoffset = transform.position.y + distancianivel;
            Vector3 posicionPersonaje = transform.position;
            Vector3 offset = new Vector3(0f, distancianivel, 0f);
            Vector3 position = posicionPersonaje + offset;
            GameObject nivelado = Instantiate(nivelador, position, Quaternion.identity);
        }
        else if (nivelescreados > 3)
        {
            Transform primerNivel = transform.GetChild(0); // Obtener el primer nivel creado
            Destroy(primerNivel.gameObject); // Destruir el primer nivel
            nivelescreados--;
        }

    }






    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("enemigo") == true)
        {
            StartShake();

        }
        if (other.gameObject.CompareTag("Cacamara1") == true)
        {
            invertircamara();
            Destroy(other.gameObject);

        }
        if (other.gameObject.CompareTag("Cacamara2Origen") == true)
        {
            resetcamara();
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Cacamara3Random") == true)
        {
            randomcamara();
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Contador") == true)
        {

            savedVelocity = rb.velocity;

            rb.isKinematic = true;

            Vector3 newPosition = InicioPosicion;
            yo.transform.position = newPosition;
            rb.isKinematic = false;
            rb.velocity = savedVelocity;

            Vector3 NuevaPosicion = InicioLava;
            lava.transform.position = NuevaPosicion;


            //if (elapsedtime >= tiempoespera)
            //{
            //    elapsedtime = 0;
            //    crearnivel();
            //    nivelescreados++;
            //    Destroy(other.gameObject);
            //}
            //else
            //{
            //    return;
            //}


        }
    }

}
