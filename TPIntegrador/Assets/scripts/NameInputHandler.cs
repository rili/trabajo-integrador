using UnityEngine;
using UnityEngine.SceneManagement;

public class NameInputHandler : MonoBehaviour
{
    public TMPro.TMP_InputField inputField;
    public int musnum;

    public void SaveNameAndRecord()
    {
        if (GameManager.topRecords.Count < 5 || GameManager.cuenta > GameManager.topRecords[4].record)
        {
            string playerName = inputField.text;
            PlayerData newPlayerData = new PlayerData(playerName, GameManager.cuenta);
            GameManager.topRecords.Add(newPlayerData);
            GameManager.topRecords.Sort((a, b) => b.record.CompareTo(a.record));
            int maxRecords = 5;
            GameManager.topRecords = GameManager.topRecords.GetRange(0, Mathf.Min(GameManager.topRecords.Count, maxRecords));
            GameManager.SaveTopRecords();

        }
        int indiceEscenaActual = SceneManager.GetActiveScene().buildIndex;

        musnum = MusicaFondo.MusNum;
        switch (musnum)
        {
            case 1:
                Parar("dalezeldadale");
                break;
            case 2:
                Parar("quevedo");
                break;
            case 3:
                Parar("whatislove");
                break;
            case 4:
                Parar("rollinggirl");
                break;
            default:
                Debug.Log("numero invalido");
                break;

        }

        SceneManager.LoadScene(indiceEscenaActual);
    }

    private void Parar(string nombre)
    {
        GestorAudio.instancia.PausarSonido(nombre);
    }
}

