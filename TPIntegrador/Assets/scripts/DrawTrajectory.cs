using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class DrawTrajectory : MonoBehaviour
{
    public float forceMultiplier;
    public int segments;
    public float drawingTimeScale = 0.5f;
    public LineRenderer lineRenderer;


    private float originalTimeScale;
    private Vector3 mousePressDownPos;
    private Vector3 forceVector;
    private bool isDrawing = false;

    private void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = segments;
        lineRenderer.enabled = false;
        originalTimeScale = Time.timeScale;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isDrawing = true;
            mousePressDownPos = Input.mousePosition;
            lineRenderer.enabled = true;

        }

        if (Input.GetMouseButton(0) && isDrawing && (DragAndShoot.disparos <= 1))
        {
            Time.timeScale = drawingTimeScale;
            Vector3 forceInit = (Input.mousePosition - mousePressDownPos);
            forceVector = new Vector3(forceInit.x, forceInit.y, 0) * forceMultiplier;
            DrawTrajectory1();
        }

        if (Input.GetMouseButtonUp(0))
        {
            isDrawing = false;
            lineRenderer.enabled = false;
            Time.timeScale = 1;
        }

        if (DragAndShoot.disparos > 1)
        {
            lineRenderer.enabled = false;
        }
    }

    private void DrawTrajectory1()
    {
        Vector3 velocity = (forceVector / GetComponent<Rigidbody>().mass) * Time.fixedDeltaTime;
        float timeStep = Time.fixedDeltaTime * 2f;
        List<Vector3> linePoints = new List<Vector3>();

        for (int i = 0; i < segments; i++)
        {
            float t = i * timeStep;
            Vector3 point = transform.position + velocity * t + 0.5f * Physics.gravity * t * t;
            linePoints.Add(point);
        }

        lineRenderer.SetPositions(linePoints.ToArray());
    }
}

