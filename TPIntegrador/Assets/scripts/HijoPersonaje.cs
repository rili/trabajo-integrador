using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HijoPersonaje : MonoBehaviour
{
    public static int hijo;
    // Start is called before the first frame update
    void Start()
    {
        hijo = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.childCount >= 2)
        {
            Transform childTransform = transform.GetChild(1);
            if (childTransform.gameObject.CompareTag("Player"))
            {
                hijo = 1;
            }
            else
            {
                hijo = 0;
            }
        }

    }
}
