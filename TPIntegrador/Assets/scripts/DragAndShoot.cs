using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DragAndShoot : MonoBehaviour
{
    public GameObject pantallagameover;
    private Vector3 mousePressDownPos;
    private Vector3 mouseReleasePos;
    private Vector3 tamañoInicial;
    private Quaternion rotacionInicial;
    public float forceMultiplier;
    public float tiempoespera;
    private float elapsedtime;
    public static int disparos;
    public float bounceForce = 10.0f;

    private Rigidbody rb;

    public static bool IsSon;

    private bool isShoot;

    void Start()
    {
        disparos = 0;
        rb = GetComponent<Rigidbody>();
        tamañoInicial = transform.localScale;
        rotacionInicial = transform.rotation;
        elapsedtime = 0f;
        IsSon = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mousePressDownPos = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            mouseReleasePos = Input.mousePosition;
            Shoot(mouseReleasePos - mousePressDownPos);

        }


        transform.rotation = rotacionInicial;
        elapsedtime++;
        //if (Input.GetKeyDown(KeyCode.R))
        //{
        //    ReiniciarEscena();
        //}
    }

   
    //private void OnMouseDown()
    //{
    //    mousePressDownPos = Input.mousePosition;
    //}

    //private void OnMouseUp()
    //{
    //    mouseReleasePos = Input.mousePosition;
    //    Shoot(mouseReleasePos - mousePressDownPos);
    //}
 
    
    void Shoot(Vector3 Force)
    {
        if (isShoot)
            return;
        rb.isKinematic = false;
        transform.SetParent(null);
        IsSon = false; 
     
        rb.AddForce(new Vector3(Force.x, Force.y, 0) * -forceMultiplier);
        GestorAudio.instancia.ReproducirSonido("menupick");

        isShoot = true;
        disparos++;
        elapsedtime = 0f;


    }

    private void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.CompareTag("piso") == true)
        {
            if (transform.parent == null)
            {
                isShoot = false;
                if (elapsedtime >= tiempoespera)
                {

                    transform.SetParent(other.transform);
                    rb.isKinematic = true;
                    IsSon = true;
                    disparos = 0;

                }

            }

        }
        if (other.gameObject.CompareTag("Rebotable"))
        {
            Vector3 contactPoint = other.contacts[0].point;

            if (contactPoint.y > other.transform.position.y)
            {
                // Rebote desde arriba
                Rigidbody rb = GetComponent<Rigidbody>();
                if (rb != null)
                {
                    Vector3 normal = other.contacts[0].normal;
                    Vector3 bounceDirection = Vector3.Reflect(rb.velocity.normalized, normal);
                    rb.velocity = bounceDirection * bounceForce;
                    disparos = 0;

                }
            }
            else
            {
                gameover();

            }
        }
    }

    public void gameover()
    {
        GestorAudio.instancia.ReproducirSonido("OOF");
        Colisionesplayer.pausa = true;
        pantallagameover.SetActive(true);
        Time.timeScale = 0;
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.CompareTag("ClickArea"))
    //    {

    //    }
    //}

    //private void ReiniciarEscena()
    //{

    //    int indiceEscenaActual = SceneManager.GetActiveScene().buildIndex;

    //    SceneManager.LoadScene(indiceEscenaActual);
    //}


}
