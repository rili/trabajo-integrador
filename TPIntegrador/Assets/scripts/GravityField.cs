using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityField : MonoBehaviour
{
    public float gravityStrength = 10f; // Fuerza de gravedad

    private void OnTriggerStay(Collider other)
    {
        // Comprobar si el objeto que choca tiene un Rigidbody
        Rigidbody rb = other.GetComponent<Rigidbody>();
        if ((rb != null) && (DragAndShoot.IsSon == false))
        {
            Vector3 direction = transform.position - other.transform.position;
            float distance = direction.magnitude;

            // Calcular la fuerza de gravedad basada en la distancia y la fuerza de gravedad
            Vector3 gravityForce = direction.normalized * (gravityStrength / (distance * distance));

            // Aplicar la fuerza al Rigidbody del objeto que choca
            rb.AddForce(gravityForce);
        }
    }
}
