using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camara : MonoBehaviour
{
    public Transform jugador;
    private Vector3 posicionInicial;
    public float offset;

    void Start()
    {
        // Almacenar la posici�n inicial de la c�mara
        posicionInicial = transform.position;
    }

    void Update()
    {
        // Obtener la posici�n actual del jugador
        Vector3 posicionJugador = jugador.position;

        // Mantener la posici�n de la c�mara en el eje X y Z
        Vector3 nuevaPosicion = new Vector3(posicionInicial.x, posicionJugador.y + offset, posicionInicial.z);

        // Actualizar la posici�n de la c�mara
        transform.position = nuevaPosicion;
    }
}
