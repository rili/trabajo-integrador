using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeleport : MonoBehaviour
{
    public float teleportIZQ;
    public float teleportDER;
    public float tiempoespera;
    private float elapsedtime;
    public float maxSpeed;
    private Rigidbody rb;
    private Vector3 savedVelocity;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        elapsedtime = 0f;
    }

    void Update()
    {
        elapsedtime++;
        if (transform.position.x > teleportDER && transform.parent == null)
        {
            elapsedtime = 0f;

            savedVelocity = rb.velocity;

            rb.isKinematic = true;

            Vector3 newPosition = transform.position;
            newPosition.x = teleportIZQ;
            transform.position = newPosition;

            if (savedVelocity.magnitude > maxSpeed)
            {
                savedVelocity = savedVelocity.normalized * maxSpeed;
            }

            rb.isKinematic = false;
            rb.velocity = savedVelocity;
        }
        if (transform.position.x < teleportIZQ && transform.parent == null)
        {
            elapsedtime = 0f;

            savedVelocity = rb.velocity;

            rb.isKinematic = true;

            Vector3 newPosition = transform.position;
            newPosition.x = teleportDER;
            transform.position = newPosition;


            if (savedVelocity.magnitude > maxSpeed)
            {
                savedVelocity = savedVelocity.normalized * maxSpeed;
            }
            rb.isKinematic = false;
            rb.velocity = savedVelocity;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ParedInvisible") == true)
        {
            if (transform.parent == null)
            {
                if (elapsedtime >= tiempoespera)
                {
                    elapsedtime = 0f;

                    savedVelocity = rb.velocity;

                    rb.isKinematic = true;

                    Vector3 newPosition = transform.position;
                    newPosition.x = teleportIZQ;
                    transform.position = newPosition;
                    if (savedVelocity.magnitude > maxSpeed)
                    {
                        savedVelocity = savedVelocity.normalized * maxSpeed;
                    }
                    rb.isKinematic = false;
                    rb.velocity = savedVelocity;

                }

                else
                {
                    return;
                }
            }
            else
            {
                return;
            }

        }

        if (other.gameObject.CompareTag("ParedInvisible2") == true)
        {
            if (transform.parent == null)
            {
                if (elapsedtime >= tiempoespera)
                {
                    elapsedtime = 0f;

                    savedVelocity = rb.velocity;

                    rb.isKinematic = true;

                    Vector3 newPosition = transform.position;
                    newPosition.x = teleportDER;
                    transform.position = newPosition;
                    if (savedVelocity.magnitude > maxSpeed)
                    {
                        savedVelocity = savedVelocity.normalized * maxSpeed;
                    }
                    rb.isKinematic = false;
                    rb.velocity = savedVelocity;

                }

                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }
    }
 
}
