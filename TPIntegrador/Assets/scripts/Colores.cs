using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colores : MonoBehaviour
{

    void Start()
    {
        GenerarColor();
    }

    void Update()
    {
        if (DragAndShoot.disparos <= 0)
        {
            GetComponent<Renderer>().sharedMaterial.color = Color.green;
        }
        if (DragAndShoot.disparos == 1)
        {
            GetComponent<Renderer>().sharedMaterial.color = Color.yellow;
        }
        if (DragAndShoot.disparos >= 2)
        {
            GetComponent<Renderer>().sharedMaterial.color = Color.red;
        }
    }

    public void GenerarColor()
    {
        GetComponent<Renderer>().sharedMaterial.color = Color.green;
    }

    public void Reset()
    {
        GetComponent<Renderer>().sharedMaterial.color = Color.green;
    }
}

