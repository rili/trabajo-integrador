using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MusicaFondo : MonoBehaviour
{
    public int NumAle;
    public static int MusNum;
    // Start is called before the first frame update
    void Start()
    {
        NumAle = Random.Range(1, 5);

        switch(NumAle)
        {
            case 1:
                Sonar("dalezeldadale");
                MusNum = 1;
                break;
            case 2:
                Sonar("quevedo");
                MusNum = 2;
                break;
            case 3:
                Sonar("whatislove");
                MusNum = 3;
                break;
            case 4:
                Sonar("rollinggirl");
                MusNum = 4;
                break;
            default:
                Debug.Log("numero invalido");
                break;

        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReiniciarEscena();
        }
    }

    private void Sonar(string nombre)
    {
        GestorAudio.instancia.ReproducirSonido(nombre);
    }

    private void Parar(string nombre)
    {
        GestorAudio.instancia.PausarSonido(nombre);
    }

    private void ReiniciarEscena()
    {
        switch (MusNum)
        {
            case 1:
                Parar("dalezeldadale");
                MusNum = 1;
                break;
            case 2:
                Parar("quevedo");
                MusNum = 2;
                break;
            case 3:
                Parar("whatislove");
                MusNum = 3;
                break;
            case 4:
                Parar("rollinggirl");
                MusNum = 4;
                break;
            default:
                Debug.Log("numero invalido");
                break;

        }


        int indiceEscenaActual = SceneManager.GetActiveScene().buildIndex;

        SceneManager.LoadScene(indiceEscenaActual);
    }
}
