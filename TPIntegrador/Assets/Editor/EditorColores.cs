using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Colores))]
public class EditorColores : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Colores caja = (Colores)target;

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Generar Color"))
        {
            caja.GenerarColor();
        }

        if (GUILayout.Button("Reset"))
        {
            caja.Reset();
        }

        GUILayout.EndHorizontal();
    }
}
